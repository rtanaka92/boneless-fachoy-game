﻿using UnityEngine;
using System.Collections;

public class SoundEffectsVolumeControl : MonoBehaviour {

	public  AudioClip musicClip;
	private  AudioSource musicSource;
	private int volumeTmp;
	public bool si;

	// Use this for initialization

	void Awake()
	{

	}

	void Start () {

		musicSource = GetComponent<AudioSource> ();
		musicSource.clip = musicClip;
		/*if (si == true) {

		if (PlayerPrefs.HasKey ("FXVolume")) {
			PlayerPrefs.SetInt("FXVolume", volumeTmp = 1);
			PlayerPrefs.Save ();	
		}
		musicSource.volume = volumeTmp;
		}/*
	}

	void OnLevelWasLoaded(int levelSound)
	{
	    //StartCoroutine ("AumentaLevel");
		levelSound++;
		si = false;
		Debug.Log ("SoundLevel  :" + levelSound);
		/*if (levelSound == 2) {
			si = true;
		} else {
			si = false;
		}*/
		//Debug.Log ("Si SOund   " + si);
	}

	// Update is called once per frame
	void Update () {
		
		if(PlayerPrefs.HasKey("FXVolume")){
			volumeTmp = PlayerPrefs.GetInt ("FXVolume");
		}
		if (volumeTmp == 0) {
			musicSource.mute = false;
		} else {
			musicSource.mute = true;
		}
		//Debug.Log ("ActivaSoundFx : " +  volumeTmp);
	}

	public void LoadingCargado()
	{
		/*Debug.Log ("SonidoOn");
			if (PlayerPrefs.HasKey ("FXVolume")) {
				
				PlayerPrefs.SetInt("FXVolume", volumeTmp = 1);
				PlayerPrefs.Save ();	
			}
			musicSource.volume = volumeTmp;*/
	}

	void OnApplicationQuit() {
		
		//Debug.Log ("SonidoSeActivaaaa");
		//si = true;
		if (PlayerPrefs.HasKey ("FXVolume")) {
			PlayerPrefs.SetInt("FXVolume", volumeTmp = 0);
			PlayerPrefs.Save ();	
		}
		musicSource.volume = volumeTmp;
	}

}
