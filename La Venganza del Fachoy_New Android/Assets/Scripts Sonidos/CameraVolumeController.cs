﻿using UnityEngine;
using System.Collections;

public class CameraVolumeController : MonoBehaviour {

	public AudioClip musicClip;
	private  AudioSource musicSource;
	private int volumeTmp;
	// Use this for initialization
	public bool siSound;


	void Awake()
	{
		
	}

	void Start () {
		musicSource = GetComponent<AudioSource> ();
		musicSource.clip = musicClip;
		/*if (siSound == true) {
			if (PlayerPrefs.HasKey ("MusicVolume")) {
				PlayerPrefs.SetInt ("MusicVolume", volumeTmp = 1);
				PlayerPrefs.Save ();	
			}
			musicSource.volume = volumeTmp;
		}*/
	
	}

//	void OnLevelWasLoaded(int levelMusic)
//	{
//		//StartCoroutine ("AumentaLevel");
//		siSound = false;
//		levelMusic++;
//		Debug.Log ("MusicLevel    :" +  levelMusic);
//	}

	void Update () {
		
		if(PlayerPrefs.HasKey("MusicVolume")){
			volumeTmp = PlayerPrefs.GetInt ("MusicVolume");
		}
		if (volumeTmp == 0) {
			musicSource.mute = false;
		} else {
			musicSource.mute = true;
		}
		//Debug.Log ("ActivaSoundMusic :" +  volumeTmp);

	}

	public void LoadingCargado()
	{
		/*Debug.Log ("MusicaOn");
			if(PlayerPrefs.HasKey("MusicVolume")){
				
				PlayerPrefs.SetInt("MusicVolume", volumeTmp = 1);
				PlayerPrefs.Save ();	
			}
			musicSource.volume = volumeTmp;*/

	}


	void OnApplicationQuit() {
		
		//Debug.Log ("MusicaSeActivaaaa");
		//siSound = true;
		if(PlayerPrefs.HasKey("MusicVolume")){
			PlayerPrefs.SetInt("MusicVolume", volumeTmp = 0);
			PlayerPrefs.Save ();	
		}
		musicSource.volume = volumeTmp;
	}



}
