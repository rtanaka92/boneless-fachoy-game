﻿using UnityEngine;
using System.Collections;

public class EnableScript : MonoBehaviour {
	public string name;
	public SpriteRenderer spr;
	public GameObject sombra;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt(name,0)==1)
		{
			spr.enabled=true;
			sombra.SetActive(false);
		}	
	}
}
