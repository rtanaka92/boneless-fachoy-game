﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CharacterSelector: MonoBehaviour {
    
    public GameObject[] characters;
    public float totalAnimationTime = 1.0f;
    //public float radius = 3.0f;
    public float a = 4; // altura en el eje y
    public float b = 6; // ancho en el eje x
    
    int direction;
    bool inTween;

    float currentAnimationTime;
    
    float angleVariation;
    float goalAnglePerCharacter;
    float[] tParameters;

    int centerCharacterIndex;
    
	public int characterNumber;
//
//	public AudioClip[] Personajes;
//	private AudioSource personajesS;


	//private int directionTmp;

	float[] tParametersTmp;
 	// Use this for initialization
	void Awake(){

        //this.transform.position = new Vector2(0f, -0.67f);
        if(!PlayerPrefs.HasKey("donhaienable"))
        {
        	PlayerPrefs.SetInt("donhaienable",0);
        	PlayerPrefs.SetInt("maestroenable",0);
        	PlayerPrefs.SetInt("sajofamenable",0);
        	PlayerPrefs.SetInt("siucaoenable",0);
        }
        	
        	//PlayerPrefs.SetInt("MisMoneditas",1000);

        characterNumber = 0;
		if (PlayerPrefs.HasKey ("CharacterNumber")) {//BORRAR SI ES NECESARIO
			PlayerPrefs.SetInt ("CharacterNumber", characterNumber);
			//PlayerPrefs.Save ();
		} else {
			PlayerPrefs.SetInt("CharacterNumber",characterNumber);
			//	PlayerPrefs.Save ();
		}
		PlayerPrefs.Save ();


	}
	void Start () {
				GameObject.Find("Costo").GetComponent<Text>().text="";

		//personajesS = GetComponent<AudioSource> ();

        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);

        inTween = false;

        goalAnglePerCharacter = 360.0f / characters.Length;

        angleVariation = goalAnglePerCharacter / totalAnimationTime;
                
        tParameters = new float[characters.Length];

		tParametersTmp  = new float[5];


        centerCharacterIndex = 0;

        float firstTparameter = 270.0f;

        for (int i = 0; i < characters.Length; ++i){
            tParameters[i] = firstTparameter - i * goalAnglePerCharacter;
			characters[i].transform.position = new Vector3(b * Mathf.Cos(tParameters[i] * Mathf.Deg2Rad), a * Mathf.Sin(tParameters[i] * Mathf.Deg2Rad), a * Mathf.Sin(tParameters[i] * Mathf.Deg2Rad));
            
            changeOpacity(i, (tParameters[i] == 270.0f ? 1 : 0.5f), 0);
        }

        direction = 1;        
    }

    // Update is called once per frame
    void Update () {

			if (!inTween){
				/*
            if(Input.GetKeyUp(KeyCode.LeftArrow)){
                initAnimation(1);
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow)){
                initAnimation(-1);
            }
            */
			}else{
				GameObject.Find("Costo").GetComponent<Text>().text="";
				GameObject.Find("Monedas Actuales").GetComponent<Text>().text="";
				currentAnimationTime += Time.deltaTime;

				for (int i = 0; i < characters.Length; ++i){
					tParameters[i] += angleVariation * Time.deltaTime * direction;
				}

				if(currentAnimationTime >= totalAnimationTime){
					inTween = false;

					currentAnimationTime = totalAnimationTime;

					for (int i = 0; i < characters.Length; ++i) {
						tParameters [i] = tParametersTmp [i] + (72 * direction);
//					print(tParametersTmp [i]);
//					print ("direction:" + direction);
					}
					if(PlayerPrefs.HasKey("CharacterNumber")){
					switch(PlayerPrefs.GetInt ("CharacterNumber"))
					{
						case 1: 
							if(PlayerPrefs.GetInt("donhaienable",0)==0)
							{
								GameObject.Find("Monedas Actuales").GetComponent<Text>().text="Tienes "+PlayerPrefs.GetInt("MisMoneditas",0)+" monedas";
								GameObject.Find("Costo").GetComponent<Text>().text="Este personaje cuesta 1 monedas";
							}
						break;
						case 2: 
							if(PlayerPrefs.GetInt("maestroenable",0)==0)
							{
								GameObject.Find("Monedas Actuales").GetComponent<Text>().text="Tienes "+PlayerPrefs.GetInt("MisMoneditas",0)+" monedas";
								GameObject.Find("Costo").GetComponent<Text>().text="Este personaje cuesta 1 monedas";
							}
						break;
						case 3: 
							if(PlayerPrefs.GetInt("sajofamenable",0)==0)
							{
								GameObject.Find("Monedas Actuales").GetComponent<Text>().text="Tienes "+PlayerPrefs.GetInt("MisMoneditas",0)+" monedas";
								GameObject.Find("Costo").GetComponent<Text>().text="Este personaje cuesta 1 monedas";
							}
						break;
						case 4: 
							if(PlayerPrefs.GetInt("siucaoenable",0)==0)
							{
								GameObject.Find("Monedas Actuales").GetComponent<Text>().text="Tienes "+PlayerPrefs.GetInt("MisMoneditas",0)+" monedas";
								GameObject.Find("Costo").GetComponent<Text>().text="Este personaje cuesta 1 monedas";
							}
						break;
					}
				}

				}

				for (int i = 0; i < characters.Length; ++i){
				characters[i].transform.localPosition = new Vector3(b * Mathf.Cos(tParameters[i] * Mathf.Deg2Rad), a * Mathf.Sin(tParameters[i] * Mathf.Deg2Rad),  a * Mathf.Sin(tParameters[i] * Mathf.Deg2Rad));  
			
				}
		

			}     
		}

    void initAnimation(int direction){


		//directionTmp = direction;

		if (!inTween) {
			
			for(int i = 0; i < tParameters.Length; ++i)
			{
				tParametersTmp[i] = tParameters[i];
			}
		
			string arrayValues = "";
			for(int i = 0; i < characters.Length; ++i){	
				arrayValues += tParametersTmp[i] +" ";
			}
			//print (arrayValues);

			inTween = true;
			currentAnimationTime = 0.0f;
        
			this.direction = direction;

			changeOpacity (centerCharacterIndex, 0.5f, 0.5f);

			centerCharacterIndex += direction;

			if (centerCharacterIndex == characters.Length) {
				centerCharacterIndex = 0;
			} else if (centerCharacterIndex < 0) {
				centerCharacterIndex = characters.Length - 1;
			}

			changeOpacity (centerCharacterIndex, 1, 0.5f);


			switch (centerCharacterIndex) {
			case 0: 
				characterNumber = 0;
				//Invoke ("SonidoPersonaje",0.5f);
				break;
			case 1: 
				characterNumber = 1;
				//personajesS = Personajes[];
				//Invoke ("SonidoPersonaje",0.5f);
				break;
			case 2: 
				characterNumber = 2;
				//Invoke ("SonidoPersonaje",0.5f);
				break;
			case 3: 
				characterNumber = 3;
				break;
			case 4: 
				characterNumber = 4;
				//Invoke ("SonidoPersonaje",0.5f);
				break;
			}
			if (PlayerPrefs.HasKey ("CharacterNumber")) {//BORRAR SI ES NECESARIO
				PlayerPrefs.SetInt ("CharacterNumber", characterNumber);
				//PlayerPrefs.Save ();
			} else {
				PlayerPrefs.SetInt ("CharacterNumber", characterNumber);
				//	PlayerPrefs.Save ();
			}
			PlayerPrefs.Save ();
			//print (characterNumber);
		}
	}

    void changeOpacity(int indexCharacter, float opacity, float time){
        SpriteRenderer centerIn = characters[indexCharacter].GetComponent<SpriteRenderer>();
		DOTween.To(() => centerIn.color, x => centerIn.color = x, new Color(centerIn.color.r, centerIn.color.g, centerIn.color.b, 1), time);
		//DOTween.To(() => centerIn.color, x => centerIn.color = x, new Color(0, 0, 0, opacity), time);
	
    }

	/*void SonidoPersonaje(){
		if (characterNumber == 0) {
			personajesS.clip = Personajes[0];
			personajesS.Play ();
		}

		if (characterNumber == 1) {
			personajesS.clip = Personajes[1];
			personajesS.Play ();
		}

		if (characterNumber == 2) {
			personajesS.clip = Personajes[2];
			personajesS.Play ();
		}

		if (characterNumber == 4) {
			personajesS.clip = Personajes[3];
			personajesS.Play ();
		}
			
	}*/
    
}
