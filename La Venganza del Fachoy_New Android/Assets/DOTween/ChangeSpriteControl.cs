﻿using UnityEngine;
using System.Collections;

public class ChangeSpriteControl : MonoBehaviour {

	public GameObject firstScenary;
	public GameObject secondScenary;
	// Use this for initialization
	void Start () {
		firstScenary.SetActive (true);
		secondScenary.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
