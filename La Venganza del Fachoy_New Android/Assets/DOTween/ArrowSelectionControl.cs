﻿using UnityEngine;
using System.Collections;

public class ArrowSelectionControl : MonoBehaviour {

	public CharacterSelector characterSelectorTmp;
	public float moveDirection;
	public AudioClip flechitaSonido;
	AudioSource flechitaSonidoSo;
	// Use this for initialization
	void Start () {
		characterSelectorTmp = (CharacterSelector)GameObject.Find ("CharacterContainerScript").GetComponent<CharacterSelector> ();
		flechitaSonidoSo = GetComponent<AudioSource> ();
		flechitaSonidoSo.clip = flechitaSonido;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ChangePlayer(){

		flechitaSonidoSo.Play ();
		characterSelectorTmp.SendMessage ("initAnimation", moveDirection);
	}
}
