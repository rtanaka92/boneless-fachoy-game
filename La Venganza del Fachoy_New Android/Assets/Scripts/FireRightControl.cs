﻿using UnityEngine;
using System.Collections;

public class FireRightControl : MonoBehaviour {



	public bool fireVisible;
	private SpriteRenderer fireSprite;
    TutorialFachoy tuto;
    PlayerControl control;
    //private PlayerControl mueveteR;
    // Use this for initialization

    void Awake()
    {
        tuto = GameObject.FindObjectOfType<TutorialFachoy>();
        control = GameObject.FindObjectOfType<PlayerControl>();
    }


	void Start () {
		fireVisible = false;
		fireSprite = GetComponent<SpriteRenderer> ();
		//mueveteR = (PlayerControl)GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerControl> ();
    }
	
	// Update is called once per frame
	void Update () {

		if (PlayerControl.noCamFollow == false) {
            gameObject.SetActive(false);
		}


        //if (!control.canAttackRight)
        //{
        //    fireSprite.enabled = false;
        //}
        //else
        //{
        //    fireSprite.enabled = true;
        //    if (tuto.enabled)
        //    {
        //        tuto.VerTutoFachoy(1f, 1f);
        //        tuto.StartCoroutine("AcaboTuto");
        //        PlayerPrefs.SetInt("TutoFachoy", 1);
        //    }
        //}


        if (fireVisible == false)
        {
            fireSprite.enabled = false;
        }
        else
        {
            fireSprite.enabled = true;
        }
    }
	void OnTriggerEnter2D(Collider2D fireRange){
		
		if (fireRange.gameObject.tag == "FireRangeRight") {
            fireVisible = true;
            if (tuto.enabled)
            {
                tuto.VerTutoFachoy(1f, 1f);
                tuto.StartCoroutine("AcaboTuto");
                PlayerPrefs.SetInt("TutoFachoy", 1);
            } 
            //Debug.Log("MeAcercoNinjaDer");
        }

	}

	void OnTriggerExit2D(Collider2D fireRange){
		if (fireRange.gameObject.tag == "FireRangeRight") {
            fireVisible = false;
        }
	}
}
