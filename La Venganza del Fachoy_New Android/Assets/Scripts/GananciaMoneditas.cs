﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GananciaMoneditas : MonoBehaviour {

    // Use this for initialization
    public Text txt_moneditas;
    public Text txt_MoneditasRequeridas;
    public Text txt_acumulaMoneditas;
    public CaiganMonedas caiganMonedas;
    bool conseguiMoneda;
    bool subeMoneda = true;
    int scoreRequerido = 0;
    int aumentaScore = 10;
    [HideInInspector]
    public ushort ganaMonedas = 0;
    public ushort totalMonedas = 0;
    public ushort moneditasRequeridas = 10;

    //ushort puestoMonedas = 0;
    //int compruebaScore;
    //int[] listaMonedas;
    GameManagerControl sumaMoneda;


    void Awake()
    {
        sumaMoneda = GetComponent<GameManagerControl>();
        
    }

    void Start() {
        txt_acumulaMoneditas.gameObject.SetActive(false);
        totalMonedas = (ushort)PlayerPrefs.GetInt("MisMoneditas");
        moneditasRequeridas = (ushort)PlayerPrefs.GetInt("MoneditasRequeridas",moneditasRequeridas);
        //Debug.Log("Ahora las moneditas que me va a pedir Fachoy es : " + moneditasRequeridas);
        //Debug.Log("Mis Monedas Actuales son : " + totalMonedas);
    }

    // Update is called once per frame
    void Update() {

    }
      

    public void AcumulaMoneditas()
    {
        if (subeMoneda)
        {
            scoreRequerido += aumentaScore;
            subeMoneda = false;
        }

        //Debug.Log("El scoreRequerido para ganar Moneditas es :  " + scoreRequerido);

        if (sumaMoneda.scoreValue == 50)
        {
            //Debug.Log("!Gano Dos Moneditas Extras!");
            totalMonedas += 2;
            SubeMonedita(2, true);
            caiganMonedas.DispersenseMonedas();
            //Debug.Log("Mis monedas ahora son:" + totalMonedas);
        }

        if (sumaMoneda.scoreValue % 100 == 0 && sumaMoneda.scoreValue == 100)
        {
            //Debug.Log("!Gano Cuatro Moneditas Extras!");
            totalMonedas += 4;
            SubeMonedita(4, true);
            caiganMonedas.DispersenseMonedas();
            //Debug.Log("Mis monedas ahora son:" + totalMonedas);
        }

        if (sumaMoneda.scoreValue % 100 == 0 && sumaMoneda.scoreValue > 100)
        {
            //Debug.Log("!Gano Seis Moneditas Extras!");
            totalMonedas += 6;
            SubeMonedita(6, true);
            caiganMonedas.DispersenseMonedas();
            //Debug.Log("Mis monedas ahora son:" + totalMonedas);
        }

        if (sumaMoneda.scoreValue == scoreRequerido)
        {
            caiganMonedas.DispersenseMonedas();
            conseguiMoneda = true;
            subeMoneda = true;
            ganaMonedas += 1;
            totalMonedas += ganaMonedas;
            SubeMonedita(ganaMonedas, true);
            //Debug.Log("¡¡GanoMonedita!!");
            //Debug.Log("El valor total de las Moneditas que gano es:" + totalMonedas);

        }
    
        if (conseguiMoneda)
        {
            aumentaScore += 10;
            conseguiMoneda = false;
        }
        //yield return new WaitForSeconds(0.5f);
        //yield return StartCoroutine(CreaListaMoneditas());
        
    }

    void SubeMonedita(ushort sube,bool muestraIncrementoMonedita)
    {
        txt_acumulaMoneditas.gameObject.SetActive(muestraIncrementoMonedita);
        txt_acumulaMoneditas.text = "+" + sube.ToString();
        Invoke("Apaguese", 1.5f);
    }

    void Apaguese()
    {
        txt_acumulaMoneditas.gameObject.SetActive(false);
    }

    public void TotalMoneditas()
    {
        txt_moneditas.text = totalMonedas.ToString();
        txt_MoneditasRequeridas.text = moneditasRequeridas.ToString();
        PlayerPrefs.SetInt("MoneditasRequeridas", moneditasRequeridas);
        PlayerPrefs.SetInt("MisMoneditas", totalMonedas);
    }

    void OnApplicationQuit()
    {
        //Debug.Log("TeBorrasTutoFachoy");
        PlayerPrefs.DeleteKey("MoneditasRequeridas");
    }

}
