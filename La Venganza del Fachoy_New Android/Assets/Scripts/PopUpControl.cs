﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class PopUpControl : MonoBehaviour {


	//public Transform personajes;
	private GameManagerControl gameManagerTmp;
	private Animator popUp;
  
    // Use this for initialization
    void Awake()
    {
        gameManagerTmp = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerControl>();
        popUp = GetComponent<Animator>();
    }

    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void FinishAnimation(){
	    gameManagerTmp.gameOver ();
	}

	void Agachate()
	{
        popUp.SetBool("Fu", true);
    }

	public void OtraVez(){
		popUp.SetBool ("Fu", false);
		SceneManager.LoadScene ("Level");
    }
}
