﻿using UnityEngine;
using System.Collections;

public class EnemyLeftControl : MonoBehaviour {



    float speed = 3f;
	public GameObject[] EfectosBarrido;

    public bool canMove;//IMPULSE
    public bool canRotate;//IMPULSE
	public float posYfinal;

	private Rigidbody2D rbEnemy;
	private Animator enemyLeftAnimator;
	private CameraShake cameraShakeTmp; 
	private GameManagerControl gameManagerTmp;
	private bool cambioVelocidad;

	public AudioClip[] ouchNinja;
	private AudioSource ouchNinjaS;

    // Use this for initialization
	void Awake(){
        
        PlayerControl.noCamFollow = true;
		gameObject.name = "EnemyL";
        //rbEnemy.mass = 0.5f;
    }

	void Start () {

        //Debug.Log("Inicialmente EfectosBarridoL es_:" + EfectosBarrido.Length);
        

        rbEnemy = GetComponent<Rigidbody2D> ();
		cameraShakeTmp = (CameraShake)GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraShake> ();
		gameManagerTmp = (GameManagerControl)GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManagerControl> ();


		canMove = true;//IMPULSE

		enemyLeftAnimator = GetComponent<Animator>();

		ouchNinjaS = GetComponent<AudioSource> ();



	}

	void FixedUpdate(){
		
		if (PlayerControl.canPowerUp == true){
			FlyOff ();
		}

	}

	// Update is called once per frame
	void Update () {

		if (PlayerControl.noCamFollow == false) {
			enemyLeftAnimator.SetTrigger ("Risa");

			if (canMove == true) {
				transform.position = new Vector2 (transform.position.x, posYfinal);
				rbEnemy.constraints = RigidbodyConstraints2D.FreezePosition;
			}
			GetComponent<BoxCollider2D> ().enabled = false;

		}

		if (gameManagerTmp.scoreValue >= 10 && PlayerControl.noCamFollow == true)
        {
			speed = 4f;
		}

		if (gameManagerTmp.scoreValue >= 35 && PlayerControl.noCamFollow == true)
        {
         
			speed = 5f;

		}

		if (gameManagerTmp.scoreValue >= 60 && PlayerControl.noCamFollow == true) {
        
   			speed = 6f;

		}

		if (gameManagerTmp.scoreValue >= 90 && PlayerControl.noCamFollow == true)
        {
			
			speed = 7f;

		}

		if (gameManagerTmp.scoreValue >= 120 && PlayerControl.noCamFollow == true)
		{

			speed = 8f;

		}

		if (gameManagerTmp.scoreValue >= 160 && PlayerControl.noCamFollow == true)
		{
			speed = 9f;
		}

		if (gameManagerTmp.scoreValue >= 220 && PlayerControl.noCamFollow == true)
		{
			speed = 10f;
		}
	
	    if (canMove == true) {
			rbEnemy.velocity = new Vector2 (speed, rbEnemy.velocity.y);
		}
		if (canRotate == true) {
			this.gameObject.transform.Rotate(new Vector3 (0,0,3));
		}

	}



    void FlyOff() {//IMPULSE
    
        GetComponent<BoxCollider2D>().enabled = false;//IMPULSE

        if (!PlayerControl.canPowerUp)
        {
            int ouchrandom = Random.Range(0, 2);
            ouchNinjaS.clip = ouchNinja[ouchrandom];
            ouchNinjaS.Play();

            //int efect = Random.Range(0, 2);
            //EfectosBarrido[efect].gameObject.GetComponent<SpriteRenderer>().flipX = true;
            //Instantiate(EfectosBarrido[efect], new Vector2(transform.position.x - 1f, transform.position.y - 1f), transform.rotation);


        }

        if (!PlayerControl.nBarridos)
        {
            int efect = Random.Range(0, 2);
            EfectosBarrido[efect].gameObject.GetComponent<SpriteRenderer>().flipX = true;
            Instantiate(EfectosBarrido[efect], new Vector2(transform.position.x - 1f, transform.position.y - 1f), transform.rotation);
        }

        //Debug.Log("Ahora efecto BarridoL es:" + EfectosBarrido.Length);

        float randomXImpulse = Random.Range(10,16);
		float randomYImpulse = Random.Range (10,16);
                             
        enemyLeftAnimator.SetTrigger ("isDead");
		cameraShakeTmp.shake = 0.01f;
		rbEnemy.velocity = Vector2.zero;//IMPULSE
		rbEnemy.AddRelativeForce (new Vector2(-randomXImpulse,randomYImpulse) , ForceMode2D.Impulse); //IMPULSE
		//GetComponent<BoxCollider2D>().isTrigger = false;//IMPULSE
		
		canMove = false;//IMPULSE
		canRotate = true;//IMPULSE
		Invoke("Die", 1f);//IMPULSE
	

	}//IMPULSE

	void Die(){//IMPULSE
		Destroy(gameObject);//IMPULSE
	}//IMPULSE

}
