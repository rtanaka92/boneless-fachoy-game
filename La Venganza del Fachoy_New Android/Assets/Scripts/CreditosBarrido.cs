﻿using UnityEngine;
using System.Collections;

public class CreditosBarrido : MonoBehaviour {

	// Use this for initialization
	public AudioClip barrido;
	AudioSource barridoS;


	void Start () {
	
		barridoS = GetComponent<AudioSource> ();
		barridoS.clip = barrido;
	}
	
	// Update is called once per frame
	public void BarridoEntrada () {
		barridoS.Play ();
	}

	public void BarridoSalida () {
		barridoS.Play ();
	}
}
