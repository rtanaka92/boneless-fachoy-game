﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Replay : MonoBehaviour,IPointerUpHandler,IPointerDownHandler  {

	// Use this for initialization
	//Animator replay;
	public AudioClip replay;
	private AudioSource replayS;
	void Start () {
		replayS = GetComponent<AudioSource> ();
		replayS.clip = replay;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SoundReplay()
	{
		replayS.Play ();
	}
	public void OnPointerDown (PointerEventData eventData)
	{		
		//Debug.Log ("TePulsas");

	}

	public void OnPointerUp (PointerEventData eventData)
	{
		//Debug.Log ("NOtePulses");

	}
}
