﻿using UnityEngine;
using System.Collections;

public class CaiganMonedas : MonoBehaviour {

    // Use this for initialization
    public GameObject[] MoneditasCayendose;
    public Transform[] posiFachoy;
    public Vector3 posOriginal;




	void Start () {
        //SiSonTraspasables(true);
    }
	
	// Update is called once per frame
	void Update () {

        if (!PlayerControl.noCamFollow)
        {
            SiSonTraspasables(false);
        }

        foreach (Transform item in posiFachoy)
        {
            if (item.gameObject.active)
            {
                transform.position = item.transform.position + posOriginal;
            }
          
        }
        //transform.position = posiFachoy[0].transform.position + posOriginal;
        //for (int i = 0; i < posiFachoy.Length; i++)
        //{
        //    transform.position = posiFachoy[i].transform.position + posOriginal;
        //}

        if (Input.GetKeyDown(KeyCode.S))
        {          
            //GameObject platita = (GameObject)Instantiate(MoneditasCayendose[0], transform.position, transform.rotation);
            //platita.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(impulx, impulx),ForceMode2D.Impulse);
            //Destroy(platita.gameObject, 2f);
            for(int i = 0; i < MoneditasCayendose.Length; i++)
            {
                //float impulso = Random.Range(2f, 5f);
                //Debug.Log(impulso);

                if (i >= 0 && i <= 3)
                {
                    //Debug.Log("SomosElGrupo1 :" + i);
                    int impulso = Random.Range(2, 5);
                    //float impuly = Random.Range(1f, 3f);
                    GameObject platita = (GameObject)Instantiate(MoneditasCayendose[i], transform.position, transform.rotation);
                    //platita.GetComponent<Rigidbody2D>().AddForce(new Vector2(impulx, impuly), ForceMode2D.Impulse);
                    platita.GetComponent<Rigidbody2D>().velocity = Vector2.right * impulso;
                    Destroy(platita.gameObject, 2f);
                }
                //
                if (i >= 4 && i <= 7)
                {
                    //Debug.Log("SomosElGrupo2 :" + i);
                    int impulso = Random.Range(2, 5);
                    //float impuly = Random.Range(1f, 3f);
                    GameObject platita = (GameObject)Instantiate(MoneditasCayendose[i], transform.position, transform.rotation);
                    //platita.GetComponent<Rigidbody2D>().AddForce(new Vector2(impulx, impuly), ForceMode2D.Impulse);
                    platita.GetComponent<Rigidbody2D>().velocity = Vector2.left * impulso;
                    Destroy(platita.gameObject, 2f);
                }
            }
        }

    }

    public void DispersenseMonedas()
    {
       for (int i = 0; i < MoneditasCayendose.Length; i++)
        {
           

            if (i >= 0 && i <= 3)
            {
                int impulso = Random.Range(2, 5);
                GameObject platita = (GameObject)Instantiate(MoneditasCayendose[i], transform.position, transform.rotation);
                platita.GetComponent<Rigidbody2D>().velocity = Vector2.right * impulso;
                Destroy(platita.gameObject, 2f);
            }
            //
            if (i >= 4 && i <= 7)
            {              
                int impulso = Random.Range(2, 5);
                GameObject platita = (GameObject)Instantiate(MoneditasCayendose[i], transform.position, transform.rotation);
                platita.GetComponent<Rigidbody2D>().velocity = Vector2.left * impulso;
                Destroy(platita.gameObject, 2f);
            }
        }
    }


    public void SiSonTraspasables(bool tras)
    {
        foreach (GameObject mone in MoneditasCayendose)
        {
            if (mone != null) mone.GetComponent<CircleCollider2D>().enabled = tras;
        }
    }




}
