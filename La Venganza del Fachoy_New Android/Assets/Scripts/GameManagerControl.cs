﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerControl : MonoBehaviour {

	public int scoreValue = 0; //valor del score
	public Transicion activaCambioEscenas;
    
    public Text txtScore;
	public Canvas mainCanvas;
	//public Canvas gameOverCanvas;
	public Text txtScoreFinal;
    
	public Text txtHighScore;
	public int highScore;
	public Text moneditasTxt;
	public GameObject popUpGameOver;

	public GameObject playerPacPow;// para seleccionar personajes
	public GameObject playerDonHai;// para seleccionar personajes
	public GameObject playerMaestro;// para seleccionar personajes
	public GameObject playerSajoFam;// para seleccionar personajes
	public GameObject playerSiuCao;// para seleccionar personajes

    public GameObject Difuminado;
	public byte characterNumberToCreate;// para seleccionar personajes

	public CreateEnemy[] cambiosGrups;


    private GananciaMoneditas ganaMoneda;


    //public Text txtMana; //para mostrar la cantidad de duracion para el power up 23 de noviembre
    // Use this for initialization
    void Awake(){

        ganaMoneda = GetComponent<GananciaMoneditas>();

        if (PlayerPrefs.HasKey("CharacterNumber")){
			characterNumberToCreate = (byte)PlayerPrefs.GetInt ("CharacterNumber");
		}

        txtScoreFinal.gameObject.SetActive(false);
        txtHighScore.gameObject.SetActive(false);

		/*for (int i = 0; i <= 4; i++) {
			
		}*/


        switch (characterNumberToCreate){
		case 0:

			playerPacPow.gameObject.SetActive (true);
			playerDonHai.gameObject.SetActive (false);
			playerMaestro.gameObject.SetActive (false);
			playerSajoFam.gameObject.SetActive (false);
			playerSiuCao.gameObject.SetActive (false);

			break;

		case 1: 

			playerPacPow.gameObject.SetActive (false);
			playerDonHai.gameObject.SetActive (true);
			playerMaestro.gameObject.SetActive (false);
			playerSajoFam.gameObject.SetActive (false);
			playerSiuCao.gameObject.SetActive (false);

			break;

		case 2: 

			playerPacPow.gameObject.SetActive (false);
			playerDonHai.gameObject.SetActive (false);
			playerMaestro.gameObject.SetActive (true);
			playerSajoFam.gameObject.SetActive (false);
			playerSiuCao.gameObject.SetActive (false);

			break;

		case 3:

			playerPacPow.gameObject.SetActive (false);
			playerDonHai.gameObject.SetActive (false);
			playerMaestro.gameObject.SetActive (false);
			playerSajoFam.gameObject.SetActive (true);
			playerSiuCao.gameObject.SetActive (false);

			break;

		case 4:

			playerPacPow.gameObject.SetActive (false);
			playerDonHai.gameObject.SetActive (false);
			playerMaestro.gameObject.SetActive (false);
			playerSajoFam.gameObject.SetActive (false);
			playerSiuCao.gameObject.SetActive (true);

			break;
		}
	}

	void Start () {
    
		popUpGameOver.gameObject.SetActive (false);
        Difuminado.gameObject.SetActive(false);
	


        if (PlayerPrefs.HasKey("Highscore")){
			highScore = PlayerPrefs.GetInt ("Highscore");
		}



	}
	public void updateScore () {
		
		scoreValue++;
		txtScore.text = scoreValue.ToString();
          
        ganaMoneda.AcumulaMoneditas();

        if (scoreValue % 20 == 0 && scoreValue != 0)
		{
			AparicionNinjas ();
		}
                  
        if (PlayerControl.numberOfKills == 5)
        {
			EstoneadoyPoderDesatado ();
			StartCoroutine("DetenteFachoy");
         
            activaCambioEscenas.SendMessage ("CambiaEscenarioChifa");
		}

		if (PlayerControl.numberOfKills == 15)
        {
			EstoneadoyPoderDesatado ();
			StartCoroutine("DetenteFachoy");
   
            activaCambioEscenas.SendMessage ("CambiaEscenarioPuente");
		}

		if (PlayerControl.numberOfKills == 35)
        {
			EstoneadoyPoderDesatado ();
			StartCoroutine("DetenteFachoy");
         
            activaCambioEscenas.SendMessage("CambiaEscenarioTemplo");
        }

		if (PlayerControl.numberOfKills == 55 || PlayerControl.numberOfKills == 0)
		{   
			PlayerControl.numberOfKills = 0;
			EstoneadoyPoderDesatado ();
			StartCoroutine("DetenteFachoy");
           
            activaCambioEscenas.SendMessage("CambiaEscenarioBosque");

        }
			
    }

	void EstoneadoyPoderDesatado()
	{
		PlayerControl.isStuned = true;
		PlayerControl.canPowerUp = true;
        Invoke("DetenteBarrido", 0.15f);
	}

    void DetenteBarrido()
    {
        PlayerControl.nBarridos = true;
    }

	IEnumerator DetenteFachoy()
	{
		yield return new WaitForSeconds (0.9f);
		PlayerControl.isStuned = false;
		PlayerControl.canPowerUp = false;
        PlayerControl.nBarridos = false;
    }


    void AparicionNinjas(){
		
		cambiosGrups[0].max = cambiosGrups[1].max = Random.Range (1f, 6f);
		cambiosGrups[0].min = cambiosGrups[1].min = Random.Range (0.8f, 2.3f); 
		//

		bool[] gruopNinjas = new bool[]{true,false};
		int randomGroup = Random.Range (0, 2);

		cambiosGrups[0].crea3Ninjas = cambiosGrups[1].crea3Ninjas = gruopNinjas[randomGroup];
		cambiosGrups[1].crea2Ninjas = cambiosGrups[0].crea2Ninjas = gruopNinjas[randomGroup];
		//
	}


    void Update () {
	
	}

	public void gameOverPopUp(){
        Invoke("PopUpEntra", 2.3f);
        Difuminado.gameObject.SetActive(true);
	}

    void PopUpEntra()
    {
        popUpGameOver.gameObject.SetActive(true);
    }

	public void gameOver(){
		
		txtScore.gameObject.SetActive (false);
		moneditasTxt.gameObject.SetActive (true);
        txtScoreFinal.gameObject.SetActive(true);
        txtScoreFinal.text = txtScore.text;
	
		highScore = PlayerPrefs.GetInt ("Highscore");
        txtHighScore.gameObject.SetActive(true);
        txtHighScore.text = highScore.ToString();

	}

}
