﻿using UnityEngine;
using System.Collections;

public class SonidoInicio : MonoBehaviour {

	// Use this for initialization
	public AudioClip sonidoInicio;
	AudioSource fraseS;
	void Start () {
	
		fraseS = GetComponent<AudioSource> ();
		fraseS.clip = sonidoInicio;
		fraseS.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
