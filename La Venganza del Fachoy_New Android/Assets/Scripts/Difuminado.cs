﻿using UnityEngine;
using System.Collections;

public class Difuminado : MonoBehaviour {

	// Use this for initialization
	public AudioClip difuminado;
	AudioSource difuminadoS;

	void Start () {
		difuminadoS = GetComponent<AudioSource> ();
		difuminadoS.clip = difuminado;
	}
	
	// Update is called once per frame
	public void SoundBarrido() {
		difuminadoS.Play ();
	}
}
