﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Moneditas : MonoBehaviour,IPointerUpHandler,IPointerDownHandler {

	// Use this for initialization
	//Animator mone;
	public AudioClip moneSound;
	AudioSource moneS;

	void Start () {
	
		moneS = GetComponent<AudioSource> ();
		moneS.clip = moneSound;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Monedita()
	{
		moneS.Play ();
	}
	public void OnPointerDown (PointerEventData eventData)
	{		
		//Debug.Log ("TePulsas");
		//mone.SetBool ("Mon", true);
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		//Debug.Log ("NOtePulses");
		//mone.SetBool ("Mon", false);
	}
}
