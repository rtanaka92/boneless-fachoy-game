﻿using UnityEngine;
using System.Collections;

public class EfectoTrans : MonoBehaviour {

	// Use this for initialization
	public Transicion changeEscenes;
	public GameManagerControl activasEscenarios;
	public GameObject[] GeneradoresNinjas;
	public GameObject[] EnemigosNinjas;
    public CreateEnemy NinjaEnemigoR;
    public CreateEnemy NinjaEnemigoL;

	public PlayerControl[] posiFachoy;


    void Start () {
 	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DetenganseNinjas(){
		
        GeneradoresNinjas[0].gameObject.SetActive(false);
		GeneradoresNinjas[1].gameObject.SetActive(false);
		//Invoke ("AparezcanNinjas", 1.10f);
    }

	public void CambiaEscenario()
	{
		Vector3 posiIni = new Vector3(0.4f,-0.88f,-0.9f);
       

        for (int i = 0; i <= 4; i++) {
			posiFachoy [i].gameObject.transform.position = posiIni;
		}

		if (PlayerControl.numberOfKills == 5) {
            for(int i = 1; i <= 8;i++)
            {
                if(i == 3 || i == 4)
                {
                    changeEscenes.Escenarios[i].gameObject.SetActive(true);
                }
                else
                {
                    changeEscenes.Escenarios[i].gameObject.SetActive(false);
                }
            }
		}

		if (PlayerControl.numberOfKills == 15) {
            for(int e = 3; e <= 8; e++)
            {
                if(e == 5 || e == 6)
                {
                    changeEscenes.Escenarios[e].gameObject.SetActive(true);
                }
                else
                {
                    changeEscenes.Escenarios[e].gameObject.SetActive(false);
                }
            }
			
		}

		if (PlayerControl.numberOfKills == 35)
        {
            for (int t = 1; t <= 8; t++)
            {
                if (t == 7 || t == 8)
                {
                    changeEscenes.Escenarios[t].gameObject.SetActive(true);
                }
                else
                {
                    changeEscenes.Escenarios[t].gameObject.SetActive(false);
                }
            }

        }

		if (PlayerControl.numberOfKills == 55 || PlayerControl.numberOfKills == 0)
        {
			//PlayerControl.numberOfKills = 0;
            for (int b = 1; b <= 8; b++)
            {
                if (b == 1 || b == 2)
                {
                    changeEscenes.Escenarios[b].gameObject.SetActive(true);
                }
                else
                {
                    changeEscenes.Escenarios[b].gameObject.SetActive(false);
                }
            }
        }

    }

	public void AdiosEfectoTrans()
	{
		//Enemigos Bosque
		if (PlayerControl.numberOfKills == 55 || PlayerControl.numberOfKills == 0)
		{
			NinjaEnemigoR.objectToCreate = EnemigosNinjas[0];
			NinjaEnemigoL.objectToCreate = EnemigosNinjas[1];
		}

		//Enemigos Chifa  
		if (PlayerControl.numberOfKills == 5)
        {
            NinjaEnemigoR.objectToCreate = EnemigosNinjas[2];
            NinjaEnemigoL.objectToCreate = EnemigosNinjas[3];
        }
		//Enemigos Puente       
		if (PlayerControl.numberOfKills == 15)
        {
           	NinjaEnemigoR.objectToCreate = EnemigosNinjas[4];
			NinjaEnemigoL.objectToCreate = EnemigosNinjas[5];
        }
		//Enemigos Templo,por ahora son los del chifa
		if (PlayerControl.numberOfKills == 35)
        {
            NinjaEnemigoR.objectToCreate = EnemigosNinjas[7];
            NinjaEnemigoL.objectToCreate = EnemigosNinjas[6];
        }

		GeneradoresNinjas [1].gameObject.SetActive (true);
		GeneradoresNinjas [0].gameObject.SetActive (true);
					        
        this.gameObject.SetActive (false);
	}


//	void AparezcanNinjas()
//	{
//		
//	}
}
