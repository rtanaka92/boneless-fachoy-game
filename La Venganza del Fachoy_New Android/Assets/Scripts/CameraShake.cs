﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;
	//public GameObject Trans;
	// How long the object should shake for.
	public float shake = 0f;
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	float medidor = 5f;
	public Transform playerS;
	//public float distance;//distancia de la camara
	public Transform[] selectplayer;
	//public AudioClip[] frasesFachoy;
   //para que la camara siga al personaje
	Vector3 originalPos;
	int characterToFollow;

	void Awake(){

	

		if (camTransform == null){
			camTransform = GetComponent(typeof(Transform)) as Transform;
		}

		if(PlayerPrefs.HasKey("CharacterNumber")){
			characterToFollow = PlayerPrefs.GetInt ("CharacterNumber");
		}

		switch(characterToFollow){
		case 0:
			playerS = selectplayer[0];
			break;

		case 1:
			playerS = selectplayer[1];
			break;

		case 2: 
			playerS = selectplayer[2];
			break;

		case 3:
			playerS = selectplayer[3];
			break;

		case 4:
			playerS = selectplayer[4];
			break;
		}
	}

	void OnEnable(){
		originalPos = camTransform.localPosition;
	}

	void Update(){
		
		if (PlayerControl.noCamFollow == true) {
			
			if (shake > 0) {
				camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

				shake -= Time.deltaTime * decreaseFactor;
			} else {
				shake = 0f;
				camTransform.localPosition = originalPos;
			}

			this.transform.position = new Vector3 (playerS.position.x, transform.position.y, transform.position.z);
		} 



		if (Input.GetKey (KeyCode.Y)) {
			medidor = (medidor - 0.1f);
			this.GetComponent<Camera> ().orthographicSize = medidor;
		}
		if (Input.GetKey (KeyCode.U)) {
			medidor = (medidor + 0.1f);
			this.GetComponent<Camera> ().orthographicSize = medidor;
		}
	}
}