﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour {

	public string levelToLoad;
	public GameObject Barrido;
	public GameObject Barrido2;
    public GameObject PopUp;
	public GameObject Difuminado;
	public Text score;
	public Text altoScore;


	public AudioClip[] Fachoy;
	private AudioSource FachoyS;
    //GananciaMoneditas mone;

	int sonidoPer;

    void Awake()
    {
       // mone = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GananciaMoneditas>();
    }

	// Use this for initialization
	void Start () 
	{
		
		FachoyS = GetComponent<AudioSource>();
		if(this.gameObject.name == "creditos")
			Difuminado.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ActivaTransicion(){
		
		if(PlayerPrefs.HasKey("CharacterNumber")){
			switch(PlayerPrefs.GetInt ("CharacterNumber"))
			{
				case 1: 
					if(PlayerPrefs.GetInt("donhaienable",0)==0)
					{
						if(PlayerPrefs.GetInt("MisMoneditas",0)>1)
						{
							PlayerPrefs.SetInt("MisMoneditas",PlayerPrefs.GetInt("MisMoneditas",0)-1);
							PlayerPrefs.SetInt("donhaienable",1);
				GameObject.Find("Costo").GetComponent<Text>().text="";
				GameObject.Find("Monedas Actuales").GetComponent<Text>().text="";
						}
						return;
					}
				break;
				case 2: 
					if(PlayerPrefs.GetInt("maestroenable",0)==0)
					{

						if(PlayerPrefs.GetInt("MisMoneditas",0)>1)
						{
							PlayerPrefs.SetInt("MisMoneditas",PlayerPrefs.GetInt("MisMoneditas",0)-1);
							PlayerPrefs.SetInt("maestroenable",1);
				GameObject.Find("Costo").GetComponent<Text>().text="";
				GameObject.Find("Monedas Actuales").GetComponent<Text>().text="";
						}
						return;
					}
				break;
				case 3: 
					if(PlayerPrefs.GetInt("sajofamenable",0)==0)
					{
						if(PlayerPrefs.GetInt("MisMoneditas",0)>1)
						{
							PlayerPrefs.SetInt("MisMoneditas",PlayerPrefs.GetInt("MisMoneditas",0)-1);
							PlayerPrefs.SetInt("sajofamenable",1);
				GameObject.Find("Costo").GetComponent<Text>().text="";
				GameObject.Find("Monedas Actuales").GetComponent<Text>().text="";

						}
						return;
					}
				break;
				case 4: 
					if(PlayerPrefs.GetInt("siucaoenable",0)==0)
					{
						if(PlayerPrefs.GetInt("MisMoneditas",0)>10)
						{

							PlayerPrefs.SetInt("MisMoneditas",PlayerPrefs.GetInt("MisMoneditas",0)-40);
							PlayerPrefs.SetInt("siucaoenable",1);
				GameObject.Find("Costo").GetComponent<Text>().text="";
				GameObject.Find("Monedas Actuales").GetComponent<Text>().text="";
						}
						return;
					}
				break;
			}
		}

		if(PlayerPrefs.HasKey("CharacterNumber")){
			sonidoPer = PlayerPrefs.GetInt ("CharacterNumber");
		}

		switch (sonidoPer){
		case 0:
			FachoyS.clip = Fachoy[0];
			FachoyS.Play ();
			break;

		case 1: 
			FachoyS.clip = Fachoy[1];
			FachoyS.Play ();
			break;

		case 2: 
			FachoyS.clip = Fachoy[2];
			FachoyS.Play ();
			break;

		case 3:

			break;

		case 4:
			FachoyS.clip = Fachoy[3];
			FachoyS.Play ();
			break;
		}

		if (Barrido.gameObject.name == "Barrido" || Barrido2.gameObject.name == "Barrido2") {
		
			Barrido.GetComponent<BarridoInicial> ().gameObject.SetActive (true);
			Barrido2.GetComponent<BarridoInicial> ().gameObject.SetActive (true);
		}
	}

	public void JugarDeNuevoyHome()
	{
		
		if (this.gameObject.name == "Again"){

            //ushort totalMonedas = (ushort)PlayerPrefs.GetInt("MisMoneditas");
            //ushort moneditasRequeridas = (ushort)PlayerPrefs.GetInt("MoneditasRequeridas");
            ////Debug.Log("Total Monedas q consegui: " + totalMonedas);
            ////Debug.Log("MonedasRequeridas : " + moneditasRequeridas);


            //if (totalMonedas >= moneditasRequeridas)
            //{

            //    //Debug.Log("Muy bien Rufian puedes seguir jugando");
            //    PopUp.GetComponent<PopUpControl>().SendMessage("Agachate");
            //    score.gameObject.SetActive(false);
            //    altoScore.gameObject.SetActive(false);
            //    Barrido.gameObject.SetActive(true);
            //    Barrido2.gameObject.SetActive(true);
            //    totalMonedas -= moneditasRequeridas;
            //    PlayerPrefs.SetInt("MisMoneditas", totalMonedas);
            //    //Debug.Log("Ahora mis Moneditas son:" + totalMonedas);
            //    moneditasRequeridas += 30;
            //    PlayerPrefs.SetInt("MoneditasRequeridas", moneditasRequeridas);
            //    //Debug.Log("Ahora las moneditas que me va a pedir Fachoy es : " + moneditasRequeridas);

            //}
            //else
            //{
            //    Debug.Log("Lo siento Rufian no hay suficientes Monedas");
            //}
            PopUp.GetComponent<PopUpControl>().SendMessage("Agachate");
            score.gameObject.SetActive(false);
            altoScore.gameObject.SetActive(false);
            Barrido.gameObject.SetActive(true);
            Barrido2.gameObject.SetActive(true);

        }

        if (this.gameObject.name == "Home"){
        
			if (Barrido.gameObject.name == "BarridoHome" || Barrido.gameObject.name == "BarridoHome3"){
				Barrido.gameObject.SetActive (true);
				Barrido2.gameObject.SetActive (true);
			}
		}
	}

	public void ActivaCreditos(){
	
		Difuminado.gameObject.SetActive(true);
		//PopUp.SetActive(true);
		PopUp.GetComponent<Creditos> ().gameObject.SetActive (true);
		PopUp.GetComponent<Creditos> ().credits.SetBool ("C2",false);
    }

	public void TeVasCreditos(){


		PopUp.GetComponent<Creditos> ().credits.SetBool ("C2", true);
		Difuminado.gameObject.SetActive (false);
	
	}

	

}
