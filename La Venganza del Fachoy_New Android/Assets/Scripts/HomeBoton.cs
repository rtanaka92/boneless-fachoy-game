﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class HomeBoton : MonoBehaviour,IPointerDownHandler  {

	// Use this for initialization
	//Animator home;
	public AudioClip homeS;
	private AudioSource homeSo;

	void Start () {
		homeSo = GetComponent<AudioSource> ();
		homeSo.clip = homeS; 
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SoundHome()
	{
		homeSo.Play ();
	}

	public void OnPointerDown (PointerEventData eventData)
	{		
		
	}

	public void OnPointerUp (PointerEventData eventData)
	{		
		
	}

}
