﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CompartirBotones : MonoBehaviour,IPointerUpHandler,IPointerDownHandler {

	// Use this for initialization
	//Animator compa;
	public AudioClip soundCom;
	AudioSource soundCo;

	void Start () {
	
		soundCo = GetComponent<AudioSource> ();
		soundCo.clip = soundCom;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SoundCompartir()
	{
		soundCo.Play ();
	}

	public void OnPointerDown (PointerEventData eventData)
	{		
		//Debug.Log ("TePulsas");
		//compa.SetBool ("Comp", true);
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		//Debug.Log ("NOtePulses");
		//compa.SetBool ("Comp", false);
	}
}
