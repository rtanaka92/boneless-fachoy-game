﻿using UnityEngine;
using System.Collections;

public class Creditos : MonoBehaviour {

	// Use this for initialization
	public Animator credits;
	public GameObject[] BotonesFXyMusic;

	void Awake()
	{
		credits = GetComponent<Animator> ();
		
	}

	void Start () {
		//SonidoyMusica ();
		//Debug.Log ("StarCredits");
		gameObject.SetActive (false);
		credits = GetComponent<Animator> ();

	}
			
	// Update is called once per frame
	void Update () {

		int volumeMusicTmp = PlayerPrefs.GetInt("MusicVolume");
		int volumeEffectsTmp = PlayerPrefs.GetInt("FXVolume");

		if (volumeMusicTmp == 0) {
			BotonesFXyMusic[1].gameObject.SetActive (false);
			BotonesFXyMusic[0].gameObject.SetActive (true);
		} else {
			BotonesFXyMusic[1].gameObject.SetActive (true);
			BotonesFXyMusic[0].gameObject.SetActive (false);
		}
		if (volumeEffectsTmp == 0) {
			BotonesFXyMusic[2].gameObject.SetActive (true);
			BotonesFXyMusic[3].gameObject.SetActive (false);
		}else {
			BotonesFXyMusic[2].gameObject.SetActive (false);
			BotonesFXyMusic[3].gameObject.SetActive (true);
		}
    }

}
