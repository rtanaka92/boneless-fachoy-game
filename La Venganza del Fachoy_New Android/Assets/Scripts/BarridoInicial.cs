﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BarridoInicial : MonoBehaviour {

	// Use this for initialization

	private bool home2;
	public AudioClip sonBarrido;
	private AudioSource BarridoS; 
	//public Canvas CanvasInicio;

	void Awake(){
		home2 = false;
	}

	void Start () {
		
		if (this.name == "Barrido") {
			BarridoS = GetComponent<AudioSource> ();
			BarridoS.clip = sonBarrido;
			BarridoS.Play ();
			//Debug.Log (sonBarrido);
		}
		if (this.gameObject.name == "Barrido" || this.gameObject.name == "Barrido2" ) {
			gameObject.SetActive (false);
		}
		if (this.gameObject.name == "BarridoHome" || this.gameObject.name == "BarridoHome3" ) {
			gameObject.SetActive (false);
		}
		if ((this.gameObject.name == "BarridoHome2" || this.gameObject.name == "BarridoHome4") && home2 == false ) {
			gameObject.SetActive (false);
		}

		if (this.gameObject.name == "BarridoJuego" || this.gameObject.name == "BarridoJuego2" ) {
			gameObject.SetActive (false);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnLevelWasLoaded()
	{
		//Debug.Log ("NuevoBarrido");
		home2 = true;
		if (this.gameObject.name == "BarridoHome2" || this.gameObject.name == "BarridoHome4") {
			this.gameObject.SetActive (true);
		}
	}

	public void LoadLevel()
	{	  
		if (this.name == "BarridoJuego") {
			BarridoS = GetComponent<AudioSource> ();
			BarridoS.clip = sonBarrido;
			BarridoS.Play ();
			//Debug.Log (sonBarrido);
		}
   	    SceneManager.LoadScene ("Level");
	}

	public void volverHome(){
		
		if (this.name == "BarridoHome") {
			BarridoS = GetComponent<AudioSource> ();
			BarridoS.clip = sonBarrido;
			BarridoS.Play ();
			Debug.Log (sonBarrido);
		}
        PlayerPrefs.DeleteKey("MoneditasRequeridas");
		SceneManager.LoadScene ("Main");
	}

	public void NivelCargado()
	{
		
		if (this.gameObject.name == "BarridoJuego" || this.gameObject.name == "BarridoJuego3") {
			BarridoS = GetComponent<AudioSource> ();
			BarridoS.clip = sonBarrido;
			BarridoS.Play ();
			//Debug.Log (sonBarrido);
		}
		if (this.gameObject.name == "BarridoJuego" || this.gameObject.name == "BarridoJuego2") {
			//this.gameObject.SetActive (false);
			//SceneManager.LoadScene ("Level");
		}

		if (this.gameObject.name == "BarridoJuego3" || this.gameObject.name == "BarridoJuego4") {
			this.gameObject.SetActive (false);
		}
	}

	public void finBarridoHome()
	{   
		BarridoS = GetComponent<AudioSource> ();
		BarridoS.clip = sonBarrido;
		BarridoS.Play ();
		//Debug.Log (sonBarrido);
		if (this.gameObject.name == "BarridoHome2" || this.gameObject.name == "BarridoHome4" ) {
			gameObject.SetActive (false);
		}
	}


	public void ApareceCanvasInicio()
	{
		//CanvasInicio.gameObject.SetActive (true);
		//SceneManager.LoadScene ("Main");
	}

	public void DesapareceLoading()
	{
		if(this.name == "GatoPlato")
		{
			SceneManager.LoadScene ("Main");
		}

	}
}
