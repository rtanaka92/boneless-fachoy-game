﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class VerVideo : MonoBehaviour,IPointerUpHandler,IPointerDownHandler  {

	// Use this for initialization
	//Animator video;
	public AudioClip videoSound;
	AudioSource videoS;

	void Start () {
	
		videoS = GetComponent<AudioSource> ();
		videoS.clip = videoSound;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void verVideo()
	{
		videoS.Play ();
	}

	public void OnPointerDown (PointerEventData eventData)
	{		
		//Debug.Log ("TePulsas");
		//video.SetBool ("Vid", true);
	}

	public void OnPointerUp (PointerEventData eventData)
	{
		//Debug.Log ("NOtePulses");
		//video.SetBool ("Vid", false);
	}
}
