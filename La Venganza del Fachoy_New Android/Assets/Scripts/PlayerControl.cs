﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerControl : MonoBehaviour {

	public static bool noCamFollow = true;
	public static int numberOfKills;
	public static bool canPowerUp;
	public static bool isStuned = false;
    public static bool nBarridos = false;
	//public static bool aturdidoL;
    //bool aturdido;
	//public static int puñete;
	public bool canAttackLeft = false;
	public bool canAttackRight = false;
	public bool isAttackingLeft = false;
	public bool isAttackingRight = false;

	public bool isAttacking = false;


	public float detectionDistance;
	//public float speedToLost;
	public float playerSpeed;

	public ElementosInteractivos botonR;
	public ElementosInteractivos botonL;
	public ElementosInteractivos poderDesatado;

    string[] Ataques;
	int randomAtaques;

    private Animator playerAnimator;
	//public Text textAturdido;
	public Text pressSToPoweUp;

	private SpriteRenderer spriteRenderer;
	//[HideInInspector]
	private Rigidbody2D rbPlayer;
	private float directionX;
	

	private GameManagerControl gameManagerTmp;
    private GananciaMoneditas ganaMoneda;
            
	public AudioClip[] sonidoFachoyClip;
	private AudioSource sonidoFachoySource;


    public SpriteRenderer fireSprite;
    public GameObject Destello;
	public RightRangeControl palabreo;
	public LeftRangeControl palabreoFam;
    public CaiganMonedas caiganMone;

	Vector2 spriteSize;
	Vector3[] areaCoordsLeft;
	Vector3[] areaCoordRight;

    TutorialFachoy tuto;

    // Use this for initialization

    void Awake(){

        tuto = GameObject.FindObjectOfType<TutorialFachoy>();
        noCamFollow = true;
		canPowerUp = false;
		numberOfKills = 0;
		//
		rbPlayer = GetComponent<Rigidbody2D> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		playerAnimator = GetComponent<Animator> ();
		sonidoFachoySource = GetComponent<AudioSource> ();
        gameManagerTmp = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManagerControl>();
        ganaMoneda = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GananciaMoneditas>();
        //GetComponent<PlayerControl> ().enabled = true;

    }

	void Start () {
        Ataques = new string[]{ "isAttack","Patadon" ,"Cabezaso"};
		pressSToPoweUp.gameObject.SetActive (false);       
		  
		
		//enemyl = (EnemyLeftControl)GameObject.Find ("EnemyL").GetComponent<EnemyLeftControl> ();
		//enemyr = (EnemyRightControl)GameObject.Find ("EnemyR").GetComponent<EnemyRightControl> ();

		canAttackRight = isAttackingRight = isAttacking = canAttackLeft = isStuned = isAttackingLeft = false;
		spriteSize = new Vector2(spriteRenderer.sprite.texture.width / 100f, spriteRenderer.sprite.texture.height / 100f);
		areaCoordsLeft = new Vector3[2];        
		areaCoordRight = new Vector3[2];
		setLimits();
	
		//int halo = (12 >= 15) ? 4 : 2;
		//Debug.Log (halo);
	}

    private void setLimits(){
		
		areaCoordsLeft[0] = new Vector3(transform.position.x - detectionDistance - spriteSize.x / 2, transform.position.y + 1f, 0);
		areaCoordsLeft[1] = new Vector3(transform.position.x - spriteSize.x / 2 - 0.2f, transform.position.y - spriteSize.y / 2 + 0.2f, 0);

		areaCoordRight[0] = new Vector3(transform.position.x + detectionDistance + spriteSize.x / 2, transform.position.y + 1f, 0);
		areaCoordRight[1] = new Vector3(transform.position.x + spriteSize.x / 2 + 0.2f, transform.position.y - spriteSize.y / 2 + 0.2f, 0);

	}
	// Update is called once per frame

	void FixedUpdate(){


	}

	void Update () {

		if (noCamFollow == false) {
			return;
		}

		setLimits();	    
		ControlesAtaque ();
		AcercamientoNinjas ();
//		

	}

    void AcercamientoNinjas(){

        //hay algun ninja al lado izquierdo
        Collider2D[] collidersLeft = Physics2D.OverlapAreaAll(areaCoordsLeft[0], areaCoordsLeft[1]);

		foreach(var collider in collidersLeft){
			if(collider.gameObject.tag == "Enemy"){
				if(!isAttackingLeft && !isStuned){
                   
                    //TutorialFachoy.showTuto = true;
                    canAttackLeft = true;
                    //
             
                }
				else{
					canAttackLeft = false;
                }
				break;
			}
		}

	//hay algun ninja al lado derecho
	Collider2D[] collidersRight = Physics2D.OverlapAreaAll(areaCoordRight[0], areaCoordRight[1]);

		foreach(var collider in collidersRight){
			if(collider.gameObject.tag == "Enemy"){
				if(!isAttackingRight && !isStuned){
                  
                    //TutorialFachoy.showTuto = true;
                    canAttackRight = true;

				}
				else{
					canAttackRight = false;

				}
				break;
			}
		}
    }

	void ControlesAtaque()
	{
		if (!isStuned){
			if (!isAttacking){
				if (!isAttackingLeft && (Input.GetKeyDown(KeyCode.A) || botonL.pulsado)){
                    if (canAttackLeft)
					{
                        if (tuto.enabled) tuto.NoVerTutoFachoy();
                        isAttackingLeft = true;
						isAttacking = true;
						botonL.pulsado = false;
					    if (isStuned == true && (canAttackRight == true || canAttackLeft == true)) {
						    isStuned = false;
					    }
						randomAtaques = Random.Range(0, 3);
						playerAnimator.SetBool (Ataques[randomAtaques], true);
						Messenger.Broadcast ("moveLeft");
						directionX = -1;
						rbPlayer.velocity = new Vector2 (playerSpeed * directionX, rbPlayer.velocity.y);

				}else if(!canAttackRight && isStuned == false)
					{
						
					 	botonL.pulsado = false;
					    TeAturdes ();
  					}                        
				}
				if (!isAttackingRight && (Input.GetKeyDown(KeyCode.D) || botonR.pulsado)){
					if (canAttackRight)
					{
                        if (tuto.enabled)tuto.NoVerTutoFachoy();
                        isAttackingRight = true;
						isAttacking = true;
						botonR.pulsado = false;
					    if (isStuned == true && (canAttackRight == true || canAttackLeft == true)) {
						    isStuned = false;
					    }
						randomAtaques = Random.Range(0, 3);
						playerAnimator.SetBool(Ataques[randomAtaques], true);
						Messenger.Broadcast ("moveRight");
						directionX = 1;
						rbPlayer.velocity = new Vector2 (playerSpeed * directionX, rbPlayer.velocity.y);
				    } else if(!canAttackLeft && isStuned == false)
					{
						
						botonR.pulsado = false;
						TeAturdes ();
					   
				    }
				}
			}            
		}

		if(directionX == -1) {
			spriteRenderer.flipX = true;
		} else {
			spriteRenderer.flipX = false;
		}
	}

	public void EmpezoAturdision(){
		
	}

	public void AcaboAturdision(){
		
	}

    void TeAturdes()
	{
		Invoke ("ChauAturdision", 0.5f);
		isStuned = true;
		playerAnimator.SetBool("Aturdido",true);
		sonidoFachoySource.clip = sonidoFachoyClip[3];
		sonidoFachoySource.Play();
	}

	void ChauAturdision(){

	    isStuned = false;
	    playerAnimator.SetBool("Aturdido", false);

	    if ( isStuned == true && (canAttackRight == true || canAttackLeft == true)) {
		        isStuned = false;
		}
		
	}

	void OnCollisionEnter2D(Collision2D enemy){
	  
		if (enemy.gameObject.tag == "Enemy" && (isAttackingLeft == true || isAttackingRight == true ))
        {
           GolpeaFachoy(enemy);

        }
        else if (enemy.gameObject.CompareTag("Enemy") && gameObject.tag == "Player")
        {
            PerdisteFachoy(enemy);
        }

    }

    private void PerdisteFachoy(Collision2D enemy)
    {
        caiganMone.SiSonTraspasables(false);
        PlayerControl.noCamFollow = false;
        Time.timeScale = 0.4f;
        StartCoroutine("Corrutina");
        ganaMoneda.StopAllCoroutines();
        playerAnimator.SetBool("Aturdido", false);
        playerAnimator.SetTrigger("KO");
        Instantiate(Destello, transform.position, transform.rotation);

        if (enemy.gameObject.name == "EnemyR")
        {

            spriteRenderer.flipX = false;
            rbPlayer.AddForce(new Vector2(-3f, 4.2f), ForceMode2D.Impulse);
            //Invoke("Concreto", 0.1f);
        }

        if (enemy.gameObject.name == "EnemyL")
        {
            spriteRenderer.flipX = true;
            rbPlayer.AddForce(new Vector2(3f, 4.2f), ForceMode2D.Impulse);
            //Invoke("Concreto", 0.1f);
        }

        Destroy(GameObject.Find("VisualRangeLeft").gameObject);
        Destroy(GameObject.Find("VisualRangeRight").gameObject);
        //GameObject.Find ("FireActiveLeftRange").GetComponent<BoxCollider2D> ().enabled = false;
        //GameObject.Find ("FireActiveRightRange").GetComponent<BoxCollider2D> ().enabled = false;

        ganaMoneda.TotalMoneditas();
        gameManagerTmp.gameOverPopUp();


        if (this.name != "SajoFam")
        {
            sonidoFachoySource.clip = sonidoFachoyClip[4];
            sonidoFachoySource.Play();
            palabreo.SendMessage("SonidoMuerte");
        }
        else
        {
            palabreoFam.SendMessage("SonidoSajoFam");
        }

        if (PlayerPrefs.HasKey("Highscore"))
        {//BORRAR SI ES NECESARIO
            if (PlayerPrefs.GetInt("Highscore") < gameManagerTmp.scoreValue)
            {
                PlayerPrefs.SetInt("Highscore", gameManagerTmp.scoreValue);
                //PlayerPrefs.Save ();
            }
        }
        else
        {
            PlayerPrefs.SetInt("Highscore", gameManagerTmp.scoreValue);
            //	PlayerPrefs.Save ();
        }
        PlayerPrefs.Save();
    }

    private void GolpeaFachoy(Collision2D enemy)
    {
        Messenger.Broadcast("setStop");

        numberOfKills++;
        //Debug.Log (numberOfKills);
        canAttackLeft = false;
        canAttackRight = false;
        isAttacking = false;
        isAttackingRight = false;
        isAttackingLeft = false;

        playerAnimator.SetBool(Ataques[0], false);
        playerAnimator.SetBool(Ataques[1], false);
        playerAnimator.SetBool(Ataques[2], false);

        enemy.gameObject.SendMessage("FlyOff");//IMPULSE		


        gameManagerTmp.updateScore();
        rbPlayer.velocity = Vector2.zero;

        //puñeteSource.Play ();
        if (randomAtaques == 0)
        {
            sonidoFachoySource.clip = sonidoFachoyClip[0];
            sonidoFachoySource.Play();
        }
        else if (randomAtaques == 1)
        {
            sonidoFachoySource.clip = sonidoFachoyClip[1];
            sonidoFachoySource.Play();
        }
        else if (randomAtaques == 2)
        {
            sonidoFachoySource.clip = sonidoFachoyClip[2];
            sonidoFachoySource.Play();
        }
        if (this.name != "SajoFam")
        {
            palabreo.gameObject.SendMessage("SonidoPersonaje");
        }
    }

    void Concreto()
    {
        rbPlayer.gravityScale = 1;
        GetComponent<Collider2D>().isTrigger = false;
    }

	IEnumerator Corrutina()
	{
	    yield return new WaitForSeconds (1.2f);
		Time.timeScale = 1f;
			
	}

    

	public void Ataca()
	{
		//Ataque = true;
	}
				    

}