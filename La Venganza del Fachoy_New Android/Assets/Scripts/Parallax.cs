﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {
	
	public float speed = 1f;

	public bool isMove;
	public bool isQuiet;

	private float timerInMove;
	//private float dirX;
	private float currentSpeed;

	private bool isLeft;
	private bool isRight;
	void Start () {
		Messenger.AddListener("moveLeft",MovingLeft);
		Messenger.AddListener ("moveRight", MovingRight);
		Messenger.AddListener ("setStop", StopMovement);

		currentSpeed = speed;
	}
	void MovingLeft () {
		isMove = true;
		isQuiet = false;
		//dirX = -1;
		isLeft = true;
		isRight = false;
	}
	void MovingRight(){
		isMove = true;
		isQuiet = false;
		//dirX = 1;
		isLeft = false;
		isRight = true;
	}
	void StopMovement(){
		isMove = false;
		isQuiet = true;
	}
	// Update is called once per frame
	void Update () {
		if(isMove == true && isQuiet == false && isLeft == false && isRight == true){
			timerInMove += Time.deltaTime;
			speed = currentSpeed;
			GetComponent<Renderer>().material.mainTextureOffset = new Vector2(timerInMove * speed /** dirX*/,0f);
			//capturamos el renderer, luego el material, luego llegamos hasta main, agregamos vector2 para
			//poder moverlo, Time.time - myTime tecnica para movimiento de texturas
		}
		if(isMove == true && isQuiet == false && isLeft == true && isRight == false){
			timerInMove -= Time.deltaTime;
			speed = currentSpeed;
			GetComponent<Renderer>().material.mainTextureOffset = new Vector2(timerInMove * speed /** dirX*/,0f);
			//capturamos el renderer, luego el material, luego llegamos hasta main, agregamos vector2 para
			//poder moverlo, Time.time - myTime tecnica para movimiento de texturas
		}
		if (isQuiet == true && isMove == false) {
			speed = 0;
		}
	}
}
