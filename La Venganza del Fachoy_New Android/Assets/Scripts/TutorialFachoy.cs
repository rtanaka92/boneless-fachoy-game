﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialFachoy : MonoBehaviour {



    //public static bool showTuto;
    public Canvas tutoFachoy;
    public Image Plantilla;
    public Image Dedito;
    int cuenta = 0;
	// Use this for initialization
    
    void Awake()
    {
        int tuto = PlayerPrefs.GetInt("TutoFachoy");
        enabled = (tuto == 1) ? false : true;
        //Debug.Log("TutoFachoy :"+ tuto);
    }

	void Start () {
        tutoFachoy.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
    
	}


    public void VerTutoFachoy(float x,float y)
    {
        //Debug.Log("TutoActivado");
        cuenta++;
        Time.timeScale = 0;
        tutoFachoy.gameObject.SetActive(true);
        Plantilla.rectTransform.localScale = new Vector2(x, y);
    }
      

    public void NoVerTutoFachoy()
    {
        //Debug.Log("TutoDesactivado");
        Time.timeScale = 1;
        tutoFachoy.gameObject.SetActive(false);
    }

    public IEnumerator AcaboTuto()
    {
        yield return new WaitForSeconds(0.5f);
        if (cuenta >=2)
        {
            enabled = false;
        }
       
       // yield break;
    }
    
    void OnApplicationQuit()
    {
        //Debug.Log("TeBorrasTutoFachoy");
        PlayerPrefs.DeleteKey("TutoFachoy");
    }
       

}
