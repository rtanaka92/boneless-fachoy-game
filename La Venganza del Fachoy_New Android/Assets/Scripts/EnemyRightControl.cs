﻿using UnityEngine;
using System.Collections;

public class EnemyRightControl : MonoBehaviour {


	float speed = -3f;
	public GameObject[] EfectosBarrido;

    public bool canMove;//IMPULSE
    public bool canRotate;//IMPULSE
	public float posYfinal;
	private Rigidbody2D rbEnemy;
	//private bool cambioVelocidad;
	private CameraShake cameraShakeTmp; 
	private GameManagerControl gameManagerTmp;
	private Animator enemyRightAnimator;
	private SpriteRenderer spriteRendererTmp;

	// Use this for initialization

	public AudioClip[] ouchNinja;
	private AudioSource ouchNinjaS;

	void Awake() {
		PlayerControl.noCamFollow = true;
		gameObject.name = "EnemyR";
        
    }

	void Start () {
        //Debug.Log("Inicialmente EfectoBarridoR es: " + EfectosBarrido.Length);
		rbEnemy = GetComponent<Rigidbody2D> ();

		cameraShakeTmp = (CameraShake)GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraShake> ();
		gameManagerTmp = (GameManagerControl)GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManagerControl> ();

		spriteRendererTmp = GetComponent<SpriteRenderer> ();
		spriteRendererTmp.flipX = true;
		canMove = true;//IMPULSE

		enemyRightAnimator = GetComponent<Animator> ();

		ouchNinjaS = GetComponent<AudioSource> ();
	

	}
	
	// Update is called once per frame
	void FixedUpdate(){
		
		if (PlayerControl.canPowerUp == true) {
			FlyOff ();
		}

	}

	void Update () {
		
		if (PlayerControl.noCamFollow == false) {
			enemyRightAnimator.SetTrigger ("Risa");
		
			if (canMove == true) {
				transform.position = new Vector2 (transform.position.x, posYfinal);
				rbEnemy.constraints = RigidbodyConstraints2D.FreezePosition;
			}
			GetComponent<BoxCollider2D> ().enabled = false;

		}

		if (gameManagerTmp.scoreValue >= 10 && PlayerControl.noCamFollow == true) {
			speed = -4f;
		}
		if (gameManagerTmp.scoreValue >= 35 && PlayerControl.noCamFollow == true) {
         
			speed = -5f;
		
		}

		if (gameManagerTmp.scoreValue >= 60 && PlayerControl.noCamFollow == true) {
               
			speed = -6f;
		}

		if (gameManagerTmp.scoreValue >= 90 && PlayerControl.noCamFollow == true) {
			
			speed = -7f;

		}

		if (gameManagerTmp.scoreValue >= 120 && PlayerControl.noCamFollow == true) {
			
			speed = -8f;

		}

		if (gameManagerTmp.scoreValue >= 160 && PlayerControl.noCamFollow == true) {

			speed = -9f;

		}

		if (gameManagerTmp.scoreValue >= 220 && PlayerControl.noCamFollow == true) {

			speed = -10f;

		}

		if (canMove == true) {//IMPULSE
			rbEnemy.velocity = new Vector2 (speed, rbEnemy.velocity.y);//IMPULSE
		}

		if (canRotate == true) {//IMPULSE
			this.gameObject.transform.Rotate(new Vector3 (0,0,3));//IMPULSE
		}//IMPULSE
	}



	void FlyOff(){//IMPULSE

        GetComponent<BoxCollider2D>().enabled = false;

        if (!PlayerControl.canPowerUp) {
			int ouchrandom = Random.Range (0, 2);
			ouchNinjaS.clip = ouchNinja[ouchrandom];
			ouchNinjaS.Play ();

            //int efect = Random.Range(0, 2);
            //EfectosBarrido[efect].gameObject.GetComponent<SpriteRenderer>().flipX = false;
            //Instantiate(EfectosBarrido[efect], new Vector2(transform.position.x + 1f, transform.position.y - 1f), transform.rotation);


        }


        if (!PlayerControl.nBarridos)
        {
            int efect = Random.Range(0, 2);
            EfectosBarrido[efect].gameObject.GetComponent<SpriteRenderer>().flipX = false;
            Instantiate(EfectosBarrido[efect], new Vector2(transform.position.x + 1f, transform.position.y - 1f), transform.rotation);

        }

        //Debug.Log("Ahora EfectoBarridoR es: " + EfectosBarrido.Length);

        float randomXImpulse = Random.Range(10,16);
		float randomYImpulse = Random.Range (10, 16);
              
        enemyRightAnimator.SetTrigger ("isDead");
		cameraShakeTmp.shake = 0.01f;
		rbEnemy.velocity = Vector2.zero;//IMPULSE
		rbEnemy.AddRelativeForce (new Vector2(randomXImpulse,randomYImpulse) , ForceMode2D.Impulse); //IMPULSE
		//GetComponent<BoxCollider2D>().isTrigger = false;//IMPULSE
	    //IMPULSE
		canMove = false;//IMPULSE
		canRotate = true;//IMPULSE
	    Invoke("Die", 1f);//IMPULSE
		//Invoke("Diee", 0.5f);


	}//IMPULSE

	void Die() {//IMPULSE
		Destroy(gameObject);//IMPULSE
	}//IMPULSE

}
