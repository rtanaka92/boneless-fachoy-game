﻿using UnityEngine;
using System.Collections;

public class CreateEnemy : MonoBehaviour {

	public GameObject objectToCreate;
	public float min;
	public float max;
	public bool crea2Ninjas;
	public bool crea3Ninjas;

	float minPositionSecondEnemy = 15f;
	float maxPositionSecondEnemy = 25f;

	public float dirX;

	float minPositionThirdEnemy = 25f;
	float maxPositionThirdEnemy = 35f;

	public GameManagerControl gameManagerTmp;
    Vector3 posiGenerator;

	// Use this for initialization
	void Awake(){
		PlayerControl.noCamFollow = true;
	}

	void Start () {
        posiGenerator = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		if (PlayerControl.noCamFollow == false) {
			return;
		}

        if(PlayerControl.numberOfKills >= 5)
        {
            transform.position = new Vector3(posiGenerator.x, -0.85f, posiGenerator.z);
        }else {
            transform.position = posiGenerator;
        }

		if (gameManagerTmp.scoreValue >= 10){
           	crea2Ninjas = true;
		}



    }


	public void createObject(){

		if (PlayerControl.noCamFollow == false) {
			return;
		}

		Instantiate (objectToCreate, transform.position, Quaternion.identity);
		float random = Random.Range (min,max);
		Invoke ("createObject", random);
		//Debug.Log (random);
		if (crea2Ninjas == true){
				float randomPositionSecondEnemy = Random.Range (minPositionSecondEnemy, maxPositionSecondEnemy);
				Instantiate (objectToCreate, new Vector3(transform.position.x + randomPositionSecondEnemy * dirX, transform.position.y, transform.position.z),Quaternion.identity);
		}

		if (crea3Ninjas == true){
				float randomPositionThirdEnemy = Random.Range (minPositionThirdEnemy, maxPositionThirdEnemy);
				Instantiate (objectToCreate, new Vector3(transform.position.x + randomPositionThirdEnemy * dirX, transform.position.y, transform.position.z),Quaternion.identity);

		}

	}

	private void OnDisable(){
		//Debug.Log ("CancelateGenerador");
		//PlayerControl.noCamFollow = false;
		CancelInvoke ();
	}
	private void OnEnable(){
		//Debug.Log ("TeCreasGenerador");
		//PlayerControl.noCamFollow = true;
		createObject ();
	}
}
